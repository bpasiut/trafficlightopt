#include <iostream>
#include <vector>
#include <cstring>
#include <rapidjson/document.h>

using namespace std;

#ifndef OPTIMIZER_CPP_H
#define OPTIMIZER_CPP_H

class Optimizer
{
private:
    /*********************/
    /* Helping variables */
    /*********************/
    
    static const char outputJson[];
    static const uint32_t allowedLightChangesPerInters = 2;
    string dataFilename;
    size_t x_size;
    int32_t maxHSumHelper;
    vector<uint32_t> maxXIndHelper;

    /**************/
    /* Parameters */
    /**************/

    /* Number of Roads */
    uint32_t E_count;
    /* Number of Time Slots */
    uint32_t T_count;
    /* Number of Intersections */
    uint32_t J_count;
    /* Intersection amount of cars in one slot */
    uint32_t v;
    /* 0 time slot initial capacity */
    vector<uint32_t> Ho;
    /* Decision roads */
    vector<bool> Hb;
    /* Road adjacency */
    vector<vector<bool>> wb;
    /* Define with traffic lights run together in time slot */
    vector<vector<vector<int32_t>>> Tl;

    /*************/
    /* Variables */
    /*************/

    vector<vector<vector<uint32_t>>> x;
    vector<vector<vector<uint32_t>>> w;
    vector<vector<int32_t>> H;

    /**********************/
    /* Decision variables */
    /**********************/

    int32_t maxHSum;
    vector<uint32_t> maxXInd;
public:
    Optimizer(std::string dFile);
    ~Optimizer();

    bool init_parameters();
    bool init_variables();
    void start();
    void check_json_param(const rapidjson::Document &doc);

    void save_out_to_json();
};

#endif // OPTIMIZER_CPP_H
