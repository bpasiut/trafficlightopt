#include <cmath>
#include <sstream>
#include <bitset>
#include <fstream>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/writer.h>

#include "Optimizer.hpp"

using namespace rapidjson;

const char Optimizer::outputJson[] = "out_data.json";

Optimizer::Optimizer(string dFile)
{ 
    cout << "No niezle dziala" << endl;
    dataFilename = dFile;
    init_parameters();
    init_variables();
}

Optimizer::~Optimizer() {}

bool Optimizer::init_parameters()
{
    uint32_t i_itr = 0;
    uint32_t j_itr = 0;

    FILE* pFile = fopen(dataFilename.c_str(), "rb");
    char buffer[65536];
    FileReadStream json(pFile, buffer, sizeof(buffer));
    Document document;
    document.ParseStream<0, UTF8<>, FileReadStream>(json);
    fclose(pFile);

    check_json_param(document);

    E_count = document["E_count"].GetInt();
    T_count = document["T_count"].GetInt();
    J_count = document["J_count"].GetInt();
    v = document["v"].GetInt();

    const Value &objHo = document["Ho"];
    assert(objHo.IsArray());
    
    for(auto &item : objHo.GetArray())
    {
        Ho.push_back(item.GetInt());
    }

    const Value &objHb = document["Hb"];
    assert(objHb.IsArray());
    
    for(auto &item : objHb.GetArray())
    {
        Hb.push_back((bool)item.GetInt());
    }

    const Value &objwb = document["wb"];
    assert(objwb.IsArray());
    wb.resize(objwb.Size());
    for(auto &item : objwb.GetArray())
    {
        assert(item.IsArray());
        for(auto &item2 : item.GetArray())
        {
            wb[i_itr].push_back((bool)item2.GetInt());
        }
        i_itr++;
    }

    i_itr = 0;
    const Value &objTl = document["Tl"];
    assert(objTl.IsArray());
    Tl.resize(objTl.Size());
    for(auto &item : objTl.GetArray())
    {
        assert(item.IsArray());
        Tl[i_itr].resize(item.Size());
        for(auto &item2 : item.GetArray())
        {
            assert(item2.IsArray());
            for(auto &item3 : item2.GetArray())
            {
                 Tl[i_itr][j_itr].push_back(item3.GetInt() - 1);
            }
            j_itr++;
        }
        j_itr = 0;
        i_itr++;
    }


    return true;
}

bool Optimizer::init_variables()
{
    // bitset value to make sure that all possible x sets are set
    // bitset size must be known in compile time so we define big number here (this value is max number of intersections)
    uint32_t b = 0;
    bitset<512> bit(b);

    // number of different traffic lights sets 
    x_size = pow(allowedLightChangesPerInters, J_count);

    // x variable resize to proper size and set el to 0
    x.resize(x_size);
    for(auto &item : x)
    {
        item.resize(E_count);
        for(auto &item2 : item)
        {
            item2.resize(E_count);
        }
    }

    // w varibale resize to proper size nad set el to 0
    w.resize(T_count);
    for(auto &item : w)
    {
        item.resize(E_count);
        for(auto &item2 : item)
        {
            item2.resize(E_count);
        }
    }

    // H varibale resize to proper size and set el to 0
    H.resize(T_count);
    for(auto &item : H)
    {
        item.resize(E_count);
    }

    // x variable assignment
    for(uint32_t i = 0; i < x_size; i++)
    {
        for(uint32_t j=0; j < E_count; j++)
        {
            for(uint32_t k=0; k < J_count; k++)
            {
                for(uint32_t l=0; l < allowedLightChangesPerInters; l++)
                {
                    for(uint32_t m=0; m < allowedLightChangesPerInters; m++)
                    {
                        if(Tl[k][l][m] == j)
                        {
                            for(uint32_t n=0; n < allowedLightChangesPerInters; n++)
                            {
                                for(uint32_t o=0; o < allowedLightChangesPerInters; o++)
                                {
                                    if(Tl[k][n][o] != -1)
                                    {
                                        if(l % allowedLightChangesPerInters)
                                            x[i][j][Tl[k][n][o]] = (int)bit.test(k);
                                        else
                                            x[i][j][Tl[k][n][o]] = (int)(!bit.test(k));
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    bit = ++b;
    }
/*
    for(uint32_t i=0; i < x_size; i++)
    {
        cout << "TABLE: " << i << endl;
        for(uint32_t j=0; j<E_count; j++)
        {
            cout << "[";
            for(uint32_t k=0; k<E_count; k++)
            {
                cout << x[i][j][k] << " ";
            }
            cout << "]" << endl;
        }
    }

    cout << "TABLE H: " << endl;
    for(uint32_t i=0; i < T_count; i++)
    {
        cout << "[";
        for(uint32_t j=0; j<E_count; j++)
        {
            cout << H[i][j] << " ";
        }
        cout << "]" << endl;
    }
    
    cout << "TABLE Ho: " << endl;
    for(uint32_t i=0; i < 1; i++)
    {
        cout << "[";
        for(uint32_t j=0; j<E_count; j++)
        {
            cout << Ho[j] << " ";
        }
        cout << "]" << endl;
    }
*/
    return true;   
}

void Optimizer::start()
{
    maxHSum = 0;
    maxHSumHelper = 0;
    maxXInd.resize(T_count);
    maxXIndHelper.resize(T_count);

    for(uint32_t i=0; i < pow(x_size, T_count); i++)
    {
        uint32_t varXTimeSlot = 0;

        for(uint32_t j=0; j < E_count; j++)
            H[0][j] = Ho[j];

        for(uint32_t j=0; j < T_count; j++)
        {
            if(j == 0)
                varXTimeSlot = i % x_size;
            else
                varXTimeSlot = (i / (x_size * j) ) % x_size;

            maxXIndHelper[j] = varXTimeSlot;

            for(uint32_t k=0; k < E_count; k++)
            {
                for(uint32_t l=0; l < E_count; l++)
                {
                    w[j][k][l] = wb[k][l] * v * x[varXTimeSlot][k][l];
                }
            }
        }

        uint32_t addVal = 0;
        uint32_t remVal = 0;
        for(uint32_t j=1; j < T_count; j++)
        {
            for(uint32_t k=0; k < E_count; k++)
            {
                H[j][k] += H[j - 1][k];
                for(uint32_t l=0; l < E_count; l++)
                {
                    addVal = (v > w[j - 1][l][k]) ? w[j - 1][l][k] : v;
                    remVal = (v > w[j - 1][k][l]) ? w[j - 1][k][l] : v;
                    H[j][k] += addVal - remVal;
                }
            }
        }

        for(uint32_t j=0; j < T_count; j++)
        {
            cout << "H: " << j;
            cout << "[";
            for(uint32_t k=0; k < E_count; k++)
            {
                cout << H[j][k] << " ";
            }
            cout << "]" << endl;
        }

        for (uint32_t j=0; j < E_count; j++)
        {
            maxHSumHelper += H[T_count - 1][j] * (int)Hb[j];
        }
        cout << "MaxHSum[" << i << "] = " << maxHSum << endl;

        if (maxHSum < maxHSumHelper)
        {
            maxHSum = maxHSumHelper;
            for (uint32_t j=0; j < T_count; j++)
                maxXInd[j] = maxXIndHelper[j];
        }

        maxHSumHelper = 0;

        for(uint32_t j=0; j < T_count; j++)
        {
            for(uint32_t k=0; k < E_count; k++)
            {
                H[j][k] = 0;
            }
        }
    }

    for (uint32_t j=0; j < T_count; j++)
        cout << "Max index[ " << j << "] : " << maxXInd[j] << endl;
    cout << "MAX H: " << maxHSum << endl;
    save_out_to_json();
}

void Optimizer::check_json_param(const Document &doc)
{
    assert(doc.HasMember("E_count"));
    assert(doc.HasMember("J_count"));
    assert(doc.HasMember("v"));
    assert(doc.HasMember("Ho"));
    assert(doc.HasMember("Hb"));
    assert(doc.HasMember("wb"));
    assert(doc.HasMember("Tl"));
}

void Optimizer::save_out_to_json()
{
    stringstream json;
    json << "{ \"TrafficLights\" : [";

    for(uint32_t i=0; i < T_count; i++)
    {
        for(uint32_t j=0; j < x_size; j++)
        {
            if(j == maxXInd[i])
            {
                json << "[";
                for(uint32_t k=0; k < E_count; k++)
                {
                    json << "[";
                    for(uint32_t l=0; l < E_count; l++)
                    {
                        if(l == E_count -1)
                            json << x[j][k][l];
                        else
                            json << x[j][k][l] << ",";
                    }
                    if(k == E_count - 1)
                        json << "]";
                    else
                        json << "],";
                }
                if(j == maxXInd.back())
                    json << "]";
                else
                    json << "],";
            }
        }
    }

    json  << "]}";

    Document d;
    d.Parse(json.str().c_str());

    FILE * fp = fopen(outputJson, "wb");
    char writeBuffer[65536];
    FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
    Writer<FileWriteStream> writer(os);
    d.Accept(writer);

    fclose(fp);
}

