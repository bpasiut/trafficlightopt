#include <iostream>
#include "cgicc-3.2.18/cgicc/Cgicc.h"
#include <sstream>
#include <vector>
#include <fstream>
#include "rapidjson/include/rapidjson/filereadstream.h"
#include "rapidjson/include/rapidjson/filewritestream.h"
#include "rapidjson/include/rapidjson/writer.h"
#include "rapidjson/include/rapidjson/document.h"

using namespace std;
using namespace cgicc;
using namespace rapidjson;

bool is_digits(const std::string &str) {
    return str.find_first_not_of("0123456789") == std::string::npos;
}
bool is_binary(const std::string &str) {
    if(str.size() != 1){
    	return false;
    }
    return str.find_first_not_of("01") == std::string::npos;
}



int main()
{
  Cgicc formData;
  
  cout << "Content-type:text/html\r\n\r\n";
  cout << "<html>\n";
  cout << "<head>\n";
  cout << "</head>\n";
  cout << "<body>\n";

  form_iterator fi_roads_count = formData.getElement("roads_count");
  form_iterator fi_time_slots_count = formData.getElement("time_slots_count");
  form_iterator fi_intersection_count = formData.getElement("intersection_count");
  form_iterator fi_intersection_intensity = formData.getElement("intersection_intensity");
  form_iterator fi_car_count_Ho = formData.getElement("car_count_Ho");
  form_iterator fi_if_out_road = formData.getElement("if_out_road");
  form_iterator fi_if_neighbour_road = formData.getElement("if_neighbour_road");
  form_iterator fi_if_light_red_or_green = formData.getElement("if_light_red_or_green");
  
//roads count validation
	
  if( !fi_roads_count->isEmpty() && fi_roads_count != (*formData).end() )
  {
    int roads_count = stoi(**fi_roads_count);
    if(roads_count > 2 ) { 
    //	cout << "Roads count: " << **fi_roads_count << endl;
    }
    else
    {
      cout << "ERROR!!!! Liczba drog musi byc wieksza niz 2!!\n" << endl;
      return 1;
    }
  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }

//time_slots_count validation
if( !fi_time_slots_count->isEmpty() && fi_time_slots_count != (*formData).end() )
  {
    int time_slots_count = stoi(**fi_time_slots_count);
    if(time_slots_count > 0 ) { 
    //	cout << "time slots count: " << **fi_time_slots_count << endl;
    }
    else
    {
      cout << "ERROR!!!! Liczba slotow czasowych musi byc wieksza od 0!!\n " << endl;
      return 1;
    }
  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }

//intersection_count validation
if( !fi_intersection_count->isEmpty() && fi_intersection_count != (*formData).end() )
  {
    int intersection_count = stoi(**fi_intersection_count);
    if(intersection_count > 0 ) { 
    //	cout << "intersection  count: " << **fi_intersection_count << endl;
    }
    else
    {
      cout << "ERROR!!!! Liczba skrzyzowan musi byc wieksza od 0!!\n " << endl;
      return 1;
    }
  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }
//intersection_intensity validation
if( !fi_intersection_intensity->isEmpty() && fi_intersection_intensity != (*formData).end() )
  {
    int intersection_intensity = stoi(**fi_intersection_intensity);
    if(intersection_intensity > 0 ) { 
    //	cout << "intersection  intensity: " << **fi_intersection_intensity << endl;
    }
    else
    {
      cout << "ERROR!!!! Liczba samochodow musi byc wieksza od 0!!\n " << endl;
      return 1;
    }
  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }


//car_count_Ho validation
vector<string> car_count_strings;
if( !fi_car_count_Ho->isEmpty() && fi_car_count_Ho != (*formData).end() )
  {
    string input = **fi_car_count_Ho;
    
    istringstream f(input);
    string s;
    while(getline(f,s,',')){
    //	cout << s << endl;
	car_count_strings.push_back(s);
    }
    //vector<int> cars_on_roads;
    for(string str : car_count_strings){
	bool is_digit = is_digits(str);
	if(is_digit == false){
	
		cout << "ERROR!!!! Jedna z wpisanych liczb zawiera znak inny niz cyfra lub przecinek!";
		return 1;
	} else {
		//cars_on_roads.push_back(stoi(str));
	}
    }
    int roads_count = stoi(**fi_roads_count);
    if(car_count_strings.size() != roads_count){
	cout << "ERROR!!!! wielkosc tablicy powinna byc taka jak liczba drog!";
	return 1;
    }


  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }


//if_out_road validation
  vector<string> if_out_road_strings;
if( !fi_if_out_road->isEmpty() && fi_if_out_road != (*formData).end() )
  {
    string input = **fi_if_out_road;

    istringstream f(input);
    string s;
    while(getline(f,s,',')){
    	//cout << s << endl;
	if_out_road_strings.push_back(s);
    }
    //vector<int> cars_on_roads;
    for(string str : if_out_road_strings){
	bool is_bin = is_binary(str);
	if(is_bin == false){
		
		cout << "ERROR!!!! Jedna z wpisanych wartosci nie jest wartoscia binarna(0,1) lub uzyty zostal zly delimeter!";
		return 1;
	}
	
    }
    int roads_count = stoi(**fi_roads_count);
    if(if_out_road_strings.size() != roads_count){
	cout << "ERROR!!!! wielkosc tablicy powinna byc taka jak liczba drog!";
	return 1;
    }
    string zero = "0";
    for(int i=0; i < if_out_road_strings.size(); i++){
	if(if_out_road_strings[i] == zero){
		if(car_count_strings[i] != zero){
			cout << "ERROR!!!! w HB";
			return 1;
		}	
	}
    }
    for(int i=0; i < car_count_strings.size(); i++){
	if(car_count_strings[i] == zero){
		if(if_out_road_strings[i] != zero){
			cout << "ERROR!!!! w HBv2";
			return 1;
		}	
	}
    }


  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }




//if_neighbour_road validation

vector<string> out_if_neighbour_road_strings;//outside matrix
vector<string> in_if_neighbour_road_strings;//inside matrix
vector<vector<string>> final_if_neighbour_road_strings;

if( !fi_if_neighbour_road->isEmpty() && fi_if_neighbour_road != (*formData).end() )
  {
    //zewnetrzna tablica
    string input = **fi_if_neighbour_road;
    istringstream f(input);
    string s;
    while(getline(f,s,';')){
	out_if_neighbour_road_strings.push_back(s);
    }
    int roads_count = stoi(**fi_roads_count);
    if(out_if_neighbour_road_strings.size() != roads_count){
	cout << "ERROR!!!! wielkosc tablicy powinna byc taka jak liczba drog! ZEWNATRZ";
	return 1;
    }
    //wewnetrzna tablica
    for(string str : out_if_neighbour_road_strings){
    	istringstream f(str);
	string s;
	while(getline(f,s,',')){
		in_if_neighbour_road_strings.push_back(s);
	}
	for(string str : in_if_neighbour_road_strings){
		bool is_bin = is_binary(str);
		if(is_bin == false){
			
			cout << "ERROR!!!! Jedna z wpisanych wartosci nie jest wartoscia binarna(0,1) lub uzyty zostal zly delimeter!";
			return 1;
	    	}
	
    	}
	if(in_if_neighbour_road_strings.size() != roads_count){
		cout << "ERROR!!!! wielkosc tablicy powinna byc taka jak liczba drog! WEWNATRZ";
		return 1;
    	}
	final_if_neighbour_road_strings.push_back(in_if_neighbour_road_strings);
	in_if_neighbour_road_strings.clear();
    }


    string zero = "0";	
    for(int i=0; i < final_if_neighbour_road_strings.size(); i++){
	vector<string> v = final_if_neighbour_road_strings[i]; 	
	for(int j=0; j < v.size(); j++){
		if(i==j){
			if (v[j] != zero){
				cout << "ERROR!!!!!! NIE ma 0 na przekatnych";
				return 1;
			} 
		}
	}
    }
//TODO symetria 

  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }




//fi_if_light_red_or_green validation
vector<string> if_light_red_or_green_strings;
vector<string> in_if_light_red_or_green_strings;
vector<string> most_inner_if_light_red_or_green_strings;
if( !fi_if_light_red_or_green->isEmpty() && fi_if_light_red_or_green != (*formData).end() )
  {
    string input = **fi_if_light_red_or_green;
    
    istringstream f(input);
    string s;
    while(getline(f,s,';')){
    //	cout << s << endl;
	if_light_red_or_green_strings.push_back(s);
    }

    int intersection_count = stoi(**fi_intersection_count);
    if(if_light_red_or_green_strings.size() != intersection_count){
	cout << "ERROR!!!! wielkosc tablicy powinna byc taka jak liczba skrzyzowan!";
	return 1;
    }


//wewnetrzna tablica
    for(string str : if_light_red_or_green_strings){
    	istringstream f(str);
	string s;
	while(getline(f,s,':')){
    		//cout << s << endl;
		in_if_light_red_or_green_strings.push_back(s);
	}

	if(in_if_light_red_or_green_strings.size() != 2){
		cout << "ERROR!!!! bledne dane - wielkosc wewnetrznej tablicy powinna byc rowna 2";
		return 1;
    	}
	for(string str : in_if_light_red_or_green_strings){
		istringstream f(str);
		string s;
		while(getline(f,s,',')){
    			//cout << s << endl;
			most_inner_if_light_red_or_green_strings.push_back(s);
		}
		if(most_inner_if_light_red_or_green_strings.size() != 2){
			cout << "ERROR!!!! bledne dane - wielkosc najbardziej wewnetrznej tablicy powinna byc rowna 2";
			return 1;
    		}


		for(string str : most_inner_if_light_red_or_green_strings){
		bool is_dig = is_digits(str);
		if(is_dig == false){
			cout << "ERROR!!!! Jedna z wpisanych wartosci nie cyfra lub uzyty zostal zly delimeter!";
			return 1;
	    	}
	
    	}

		most_inner_if_light_red_or_green_strings.clear();
	}
	in_if_light_red_or_green_strings.clear();
    }

//TODO sprawdzanie sasiedztwa

  }
  else
  {
    cout << "Unexpected ERROR!!!!" << endl;
    return 1;
  }



//SAVE DATA TO JSON
//musi byc pusty plik data.json w lokalizacji bo inaczej nie dziala!
	stringstream json;
	json << "{\"E_count\": ";
	json << **fi_roads_count;	
	json << ", \"T_count\": ";
	json << **fi_time_slots_count;
	json << ", \"J_count\": ";
	json << **fi_intersection_count;
	json << ", \"v\": ";
	json << **fi_intersection_intensity;
	json << ", \"Ho\": [";
	json << **fi_car_count_Ho;
	json << "]";
	json << ", \"Hb\": [";
	json << **fi_if_out_road;
	json << "]";
	json << ", \"wb\": [";
	for(int i = 0; i < final_if_neighbour_road_strings.size(); i++){
		vector<string> str = final_if_neighbour_road_strings[i];	
		json << "[";
		for(int j = 0; j < str.size(); j ++){
			string s = str[j];
			json << s;
			if(j != (str.size() - 1)){
				json << ",";
			}
		}		
		json << "]";
		if(i != (final_if_neighbour_road_strings.size() - 1)){
			json << ",";
		}
	}
	json << "]";
	json << ", \"Tl\": [";
	for(int i = 0; i < if_light_red_or_green_strings.size(); i++){
		string s = if_light_red_or_green_strings[i];
		json << "[";
		int index = s.find(":");
		s = s.replace(index, 1, ",");
		s = s.insert(index, "]");
		s = s.insert(index+2,"[");
		s = "[" + s;
		
		s = s.append("]");
		
		json << s;
		json << "]";
		if(i != (if_light_red_or_green_strings.size() - 1)){
			json << ",";
		}

			
	}
	json << "]";
	json << "}";
	Document d;
	d.Parse(json.str().c_str());
	assert(d.IsObject());
	FILE * fp = fopen("data.json", "wb");
	char writeBuffer[65536];
	FileWriteStream os(fp,writeBuffer,sizeof(writeBuffer));	
	Writer<FileWriteStream> writer(os);
	d.Accept(writer);	
	fclose(fp);
	system("./optimizer > log");
	cout << "WALIDACJA DANYCH PRZEBIEGLA POPRAWNIE. Optymalizator za chwile rozpocznie prace!";
	

  cout << "<br/>\n";
  cout << "</body>\n";
  cout << "</html>\n";

  return 0;
}
