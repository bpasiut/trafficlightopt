library(shiny)
library(ggplot2)
library(DT)
library(corrplot)

shinyServer(function(input, output, session) {
  df <- reactive({
    inFiles <- input$fileIn
    if (is.null(inFiles))
      return(NULL)
    for (i in seq_along(inFiles$datapath)) {
      tmp <- jsonlite::fromJSON(inFiles$datapath[i],simplifyVector = TRUE)  # wczytywanie danych
      T <- dim(tmp$TrafficLights[])[1] # zczytaj ilosc szczelin czasowych
      a <- list()
      for (j in seq(1,T,1)) {
        a[[j]] <- t(tmp$TrafficLights[j,,]) # stwórz liste macierzy dla kazdej szczeliny
      }
      corrplot(a[[input$s]],col = "green",tl.cex=2.5,tl.col = "black",tl.srt = 0,cl.pos = "n") # wygeneruj wykres w zaleznosci od suwaka
      T
    }
  })
  
  output$SlajderXD <- renderUI({
      sliderInput(inputId="s", label = "szczelina czasowa : ", 1, 10 , step = 1,value = 1)
  })
  
  output$Corrplot <- renderPlot(
   df(), width = 650, height = 650
  )
  output$tbl2 <- DT::renderDataTable(
    input$fileIn
  )
  output$Opis = renderText(
    "Jak czytać macierz wizualizacji :  Przykładowo jeśli w wyniku na pozycji o indeksie [3][2] : 2 - druga kolumna od lewej, 3 - 
trzeci wiersz od góry ) jest zielone, oznacza to że w wybranej na suwaku szczelinie czasowej należy
zapalić zielone światło na sygnalizatorze z drogi (krawędzi) o indeksie 2 do 3. 

Indeks kolumny oznacza drogę źródłową skrzyżowania, natomiast indeks wiersza oznacza drogę wyjazdową.")
  
})